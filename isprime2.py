#-------------------------------------------------------------------------------
# Name:        module2
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     06-11-2011
# Copyright:   (c) Majibur Rahman 2011
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

    def isprime(n,PROB):
    '''returns if the number is prime. Failure rate: 1/4**PROB '''
        if n==2:
            return '1'
        if n==1 or n&1==0:
            return '0'
    s=0
    d=n-1
    while 1&d==0:
        s+=1
        d>>=1
    for i in range(PROB):
        a=random.randint(2,n-1)
        composit=True
    if pow(a,d,n)==1:
    composit=False
    if composit:
    for r in xrange(0,s):
    if pow(a,d*2**r,n)==n-1:
    composit=False
    break
    if composit: return False
    return True