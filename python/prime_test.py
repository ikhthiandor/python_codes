while 1:
    n = input('enter an integer :\t')
    if n < 2 : print 'try an integer greater than 1'; continue
    i, sqr, j = 0, long(n**0.5), 3
    if n != 2 and n & 1 == 0 : print 'not prime'
    else :
        while j < sqr :
            if not n % j: i += 1
            if i == 1 : break
            j += 2
        if i : print 'not prime'
        else : print 'prime'
