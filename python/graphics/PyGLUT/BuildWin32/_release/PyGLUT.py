#-------------------------------------------------------------------------------
# PyGLUT demo
#
# Created by Andrew H. Cox
# ahcox@btinternet.com
# andy4294967296@hotmail.com
# www.btinternet.com/~ahcox/
#
from PyGLUT import *

mouse_x = 0
mouse_y = 0

#-------------------------------------------------------------------------------
def display():
    global count
    global mouse_x, mouse_y
    print (mouse_x, mouse_y)

    #Scene:
    glutSolidSphere(0.2, 32, 32)
    glutWireTeapot(0.5)
    glutWireCube(1)
    glutWireCube(1.1)
    glutWireCube(1.2)
    glutWireCube(1.3)
    glutSolidTorus(1.08, 2, 8, 128);

    glutSwapBuffers()

#-------------------------------------------------------------------------------
def mouse(button, state, x, y):
    if state == GLUT_UP: but = "up"
    if state == GLUT_DOWN: but = "down"
    print "Mouse button pressed \nButton:", button, "State:", but, "X,Y:", (x,y)

#-------------------------------------------------------------------------------
def motion(x, y):
    global mouse_x, mouse_y
    mouse_x = x
    mouse_y = y
    print "Motion:", (x,y)

#-------------------------------------------------------------------------------
def passive_motion(x, y):
    print "Passive Motion:", (x,y)

#-------------------------------------------------------------------------------
def keyboard(key, x, y):
    print key, x, y

#-------------------------------------------------------------------------------
def menu_func(event):
    print 'Menu entry', event, 'selected'

#-------------------------------------------------------------------------------
glutInit(1, ['poop'])
glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE) # RGBA
glutInitWindowPosition(0, 0)
glutInitWindowSize(512, 512)
win_id = glutCreateWindow('PyGLUT Demo')
glutDisplayFunc(display)
glutMotionFunc(motion)
glutPassiveMotionFunc(passive_motion)
glutKeyboardFunc(keyboard)
glutMouseFunc(mouse)

glutMainLoop()

print 'Script Terminated.'
