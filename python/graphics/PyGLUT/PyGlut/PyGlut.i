//******************************************************************************
// PyGLUT.i
//
%module PyGLUT
%{
//
// This file is derived from the glut.h distributed with GLUT 3.7.
// It is a complete wrapper for GLUT API version 4 (provisional) including the
// game functionality.
//
// The conversion to a SWIG interface file was done by me:
// Andrew Cox (acox@globalnet.co.uk).
// Any correspondence about PyGLUT should be directed to me.
//
// Any updates will be made available at my homepage:
// http://www.users.globalnet.co.uk/~acox/
//
// I place no additional limitations on what can be done with the contents of
// this file beyond those it inherits from GLUT.
//
// DISCLAIMER:
// PyGLUT is provided AS IS without warranty of any kind, either express or
// implied, including but not limited to the implied warranties of
// merchantability and fitness for a particular purpose. In no event shall
// Andrew Cox be liable for any damages whatsoever including direct, indirect,
// incidental, consequential, loss of business profits or special damages,
// even if Andrew Cox has been advised of the possibility of such damages.
//

/* Copyright (c) Mark J. Kilgard, 1994, 1995, 1996, 1998. */

/* This program is freely distributable without licensing fees  and is
   provided without guarantee or warrantee expressed or  implied. This
   program is -not- in the public domain. */
//
// ToDo: 
// > Rather than refcounting menu callbacks passed in, do something less fragile
//   in the face of misuse of Glut's Create/Destroy interface.
//
%}

%{
// The default api version is set to 3 within Glut.h unless it has already been
// set outside so I set it explicitly to four (This file is hard-coded for
// version 4 but commented out #ifdefs identify the version specific contents
// correctly).
#define GLUT_API_VERSION 4
#include "gl\glut.h"

%}

// Glut Defines

/* Display mode bit masks. */
#define GLUT_RGB			0
#define GLUT_RGBA			GLUT_RGB
#define GLUT_INDEX			1
#define GLUT_SINGLE			0
#define GLUT_DOUBLE			2
#define GLUT_ACCUM			4
#define GLUT_ALPHA			8
#define GLUT_DEPTH			16
#define GLUT_STENCIL			32
//#if (GLUT_API_VERSION >= 2) // [AHC]
#define GLUT_MULTISAMPLE		128
#define GLUT_STEREO			256
//#endif
//#if (GLUT_API_VERSION >= 3) // [AHC]
#define GLUT_LUMINANCE			512
//#endif

/* Mouse buttons. */
#define GLUT_LEFT_BUTTON		0
#define GLUT_MIDDLE_BUTTON		1
#define GLUT_RIGHT_BUTTON		2

/* Mouse button  state. */
#define GLUT_DOWN			0
#define GLUT_UP				1

//#if (GLUT_API_VERSION >= 2) // [AHC]
/* function keys */
#define GLUT_KEY_F1			1
#define GLUT_KEY_F2			2
#define GLUT_KEY_F3			3
#define GLUT_KEY_F4			4
#define GLUT_KEY_F5			5
#define GLUT_KEY_F6			6
#define GLUT_KEY_F7			7
#define GLUT_KEY_F8			8
#define GLUT_KEY_F9			9
#define GLUT_KEY_F10			10
#define GLUT_KEY_F11			11
#define GLUT_KEY_F12			12
/* directional keys */
#define GLUT_KEY_LEFT			100
#define GLUT_KEY_UP			101
#define GLUT_KEY_RIGHT			102
#define GLUT_KEY_DOWN			103
#define GLUT_KEY_PAGE_UP		104
#define GLUT_KEY_PAGE_DOWN		105
#define GLUT_KEY_HOME			106
#define GLUT_KEY_END			107
#define GLUT_KEY_INSERT			108
// #endif // [AHC]

/* Entry/exit  state. */
#define GLUT_LEFT			0
#define GLUT_ENTERED			1

/* Menu usage  state. */
#define GLUT_MENU_NOT_IN_USE		0
#define GLUT_MENU_IN_USE		1

/* Visibility  state. */
#define GLUT_NOT_VISIBLE		0
#define GLUT_VISIBLE			1

/* Window status  state. */
#define GLUT_HIDDEN			0
#define GLUT_FULLY_RETAINED		1
#define GLUT_PARTIALLY_RETAINED		2
#define GLUT_FULLY_COVERED		3

/* Color index component selection values. */
#define GLUT_RED			0
#define GLUT_GREEN			1
#define GLUT_BLUE			2

/* Layers for use. */
#define GLUT_NORMAL			0
#define GLUT_OVERLAY			1


%text %{
// Original Font stuff from Glut.h: [AHC]
// Below are replacements that use simple integers.
#if defined(_WIN32)
/* Stroke font constants (use these in GLUT program). */
#define GLUT_STROKE_ROMAN		((void*)0)
#define GLUT_STROKE_MONO_ROMAN		((void*)1)

/* Bitmap font constants (use these in GLUT program). */
#define GLUT_BITMAP_9_BY_15		((void*)2)
#define GLUT_BITMAP_8_BY_13		((void*)3)
#define GLUT_BITMAP_TIMES_ROMAN_10	((void*)4)
#define GLUT_BITMAP_TIMES_ROMAN_24	((void*)5)
//#if (GLUT_API_VERSION >= 3)
#define GLUT_BITMAP_HELVETICA_10	((void*)6)
#define GLUT_BITMAP_HELVETICA_12	((void*)7)
#define GLUT_BITMAP_HELVETICA_18	((void*)8)
//#endif

#else

/* Stroke font constants (use these in GLUT program). */
#define GLUT_STROKE_ROMAN		(&glutStrokeRoman)
#define GLUT_STROKE_MONO_ROMAN		(&glutStrokeMonoRoman)

/* Bitmap font constants (use these in GLUT program). */
#define GLUT_BITMAP_9_BY_15		(&glutBitmap9By15)
#define GLUT_BITMAP_8_BY_13		(&glutBitmap8By13)
#define GLUT_BITMAP_TIMES_ROMAN_10	(&glutBitmapTimesRoman10)
#define GLUT_BITMAP_TIMES_ROMAN_24	(&glutBitmapTimesRoman24)
//#if (GLUT_API_VERSION >= 3)
#define GLUT_BITMAP_HELVETICA_10	(&glutBitmapHelvetica10)
#define GLUT_BITMAP_HELVETICA_12	(&glutBitmapHelvetica12)
#define GLUT_BITMAP_HELVETICA_18	(&glutBitmapHelvetica18)
//#endif
#endif
%}


/* Stroke font constants (use these in GLUT program). */
#define GLUT_STROKE_ROMAN		0
#define GLUT_STROKE_MONO_ROMAN	1

/* Bitmap font constants (use these in GLUT program). */
#define GLUT_BITMAP_9_BY_15		2
#define GLUT_BITMAP_8_BY_13		3
#define GLUT_BITMAP_TIMES_ROMAN_10	4
#define GLUT_BITMAP_TIMES_ROMAN_24	5
//#if (GLUT_API_VERSION >= 3)
#define GLUT_BITMAP_HELVETICA_10	6
#define GLUT_BITMAP_HELVETICA_12	7
#define GLUT_BITMAP_HELVETICA_18	8
//#endif

/* glutGet parameters. */
#define GLUT_WINDOW_X			100
#define GLUT_WINDOW_Y			101
#define GLUT_WINDOW_WIDTH		102
#define GLUT_WINDOW_HEIGHT		103
#define GLUT_WINDOW_BUFFER_SIZE		104
#define GLUT_WINDOW_STENCIL_SIZE	105
#define GLUT_WINDOW_DEPTH_SIZE		106
#define GLUT_WINDOW_RED_SIZE		107
#define GLUT_WINDOW_GREEN_SIZE		108
#define GLUT_WINDOW_BLUE_SIZE		109
#define GLUT_WINDOW_ALPHA_SIZE		110
#define GLUT_WINDOW_ACCUM_RED_SIZE	111
#define GLUT_WINDOW_ACCUM_GREEN_SIZE	112
#define GLUT_WINDOW_ACCUM_BLUE_SIZE	113
#define GLUT_WINDOW_ACCUM_ALPHA_SIZE	114
#define GLUT_WINDOW_DOUBLEBUFFER	115
#define GLUT_WINDOW_RGBA		116
#define GLUT_WINDOW_PARENT		117
#define GLUT_WINDOW_NUM_CHILDREN	118
#define GLUT_WINDOW_COLORMAP_SIZE	119
//#if (GLUT_API_VERSION >= 2)
#define GLUT_WINDOW_NUM_SAMPLES		120
#define GLUT_WINDOW_STEREO		121
//#endif
//#if (GLUT_API_VERSION >= 3)
#define GLUT_WINDOW_CURSOR		122
//#endif
#define GLUT_SCREEN_WIDTH		200
#define GLUT_SCREEN_HEIGHT		201
#define GLUT_SCREEN_WIDTH_MM		202
#define GLUT_SCREEN_HEIGHT_MM		203
#define GLUT_MENU_NUM_ITEMS		300
#define GLUT_DISPLAY_MODE_POSSIBLE	400
#define GLUT_INIT_WINDOW_X		500
#define GLUT_INIT_WINDOW_Y		501
#define GLUT_INIT_WINDOW_WIDTH		502
#define GLUT_INIT_WINDOW_HEIGHT		503
#define GLUT_INIT_DISPLAY_MODE		504
//#if (GLUT_API_VERSION >= 2)
#define GLUT_ELAPSED_TIME		700
//#endif
//#if (GLUT_API_VERSION >= 4 || GLUT_XLIB_IMPLEMENTATION >= 13)
#define GLUT_WINDOW_FORMAT_ID		123
//#endif

//#if (GLUT_API_VERSION >= 2)
/* glutDeviceGet parameters. */
#define GLUT_HAS_KEYBOARD		600
#define GLUT_HAS_MOUSE			601
#define GLUT_HAS_SPACEBALL		602
#define GLUT_HAS_DIAL_AND_BUTTON_BOX	603
#define GLUT_HAS_TABLET			604
#define GLUT_NUM_MOUSE_BUTTONS		605
#define GLUT_NUM_SPACEBALL_BUTTONS	606
#define GLUT_NUM_BUTTON_BOX_BUTTONS	607
#define GLUT_NUM_DIALS			608
#define GLUT_NUM_TABLET_BUTTONS		609
//#endif
//#if (GLUT_API_VERSION >= 4 || GLUT_XLIB_IMPLEMENTATION >= 13)
#define GLUT_DEVICE_IGNORE_KEY_REPEAT   610
#define GLUT_DEVICE_KEY_REPEAT          611
#define GLUT_HAS_JOYSTICK		612
#define GLUT_OWNS_JOYSTICK		613
#define GLUT_JOYSTICK_BUTTONS		614
#define GLUT_JOYSTICK_AXES		615
#define GLUT_JOYSTICK_POLL_RATE		616
//#endif

//#if (GLUT_API_VERSION >= 3)
/* glutLayerGet parameters. */
#define GLUT_OVERLAY_POSSIBLE           800
#define GLUT_LAYER_IN_USE		801
#define GLUT_HAS_OVERLAY		802
#define GLUT_TRANSPARENT_INDEX		803
#define GLUT_NORMAL_DAMAGED		804
#define GLUT_OVERLAY_DAMAGED		805

//#if (GLUT_API_VERSION >= 4 || GLUT_XLIB_IMPLEMENTATION >= 9)
/* glutVideoResizeGet parameters. */
#define GLUT_VIDEO_RESIZE_POSSIBLE	900
#define GLUT_VIDEO_RESIZE_IN_USE	901
#define GLUT_VIDEO_RESIZE_X_DELTA	902
#define GLUT_VIDEO_RESIZE_Y_DELTA	903
#define GLUT_VIDEO_RESIZE_WIDTH_DELTA	904
#define GLUT_VIDEO_RESIZE_HEIGHT_DELTA	905
#define GLUT_VIDEO_RESIZE_X		906
#define GLUT_VIDEO_RESIZE_Y		907
#define GLUT_VIDEO_RESIZE_WIDTH		908
#define GLUT_VIDEO_RESIZE_HEIGHT	909
//#endif

/* glutUseLayer parameters. */
//#define GLUT_NORMAL			0 // These two are #defined earlier in the file(???).
//#define GLUT_OVERLAY			1

/* glutGetModifiers return mask. */
#define GLUT_ACTIVE_SHIFT               1
#define GLUT_ACTIVE_CTRL                2
#define GLUT_ACTIVE_ALT                 4

/* glutSetCursor parameters. */
/* Basic arrows. */
#define GLUT_CURSOR_RIGHT_ARROW		0
#define GLUT_CURSOR_LEFT_ARROW		1
/* Symbolic cursor shapes. */
#define GLUT_CURSOR_INFO		2
#define GLUT_CURSOR_DESTROY		3
#define GLUT_CURSOR_HELP		4
#define GLUT_CURSOR_CYCLE		5
#define GLUT_CURSOR_SPRAY		6
#define GLUT_CURSOR_WAIT		7
#define GLUT_CURSOR_TEXT		8
#define GLUT_CURSOR_CROSSHAIR		9
/* Directional cursors. */
#define GLUT_CURSOR_UP_DOWN		10
#define GLUT_CURSOR_LEFT_RIGHT		11
/* Sizing cursors. */
#define GLUT_CURSOR_TOP_SIDE		12
#define GLUT_CURSOR_BOTTOM_SIDE		13
#define GLUT_CURSOR_LEFT_SIDE		14
#define GLUT_CURSOR_RIGHT_SIDE		15
#define GLUT_CURSOR_TOP_LEFT_CORNER	16
#define GLUT_CURSOR_TOP_RIGHT_CORNER	17
#define GLUT_CURSOR_BOTTOM_RIGHT_CORNER	18
#define GLUT_CURSOR_BOTTOM_LEFT_CORNER	19
/* Inherit from parent window. */
#define GLUT_CURSOR_INHERIT		100
/* Blank cursor. */
#define GLUT_CURSOR_NONE		101
/* Fullscreen crosshair (if available). */
#define GLUT_CURSOR_FULL_CROSSHAIR	102
//#endif


// OpenGL Types: (FixMe: remove those not used by GLUT)
typedef unsigned int GLenum;
typedef unsigned char GLboolean;
typedef unsigned int GLbitfield;
typedef signed char GLbyte;
typedef short GLshort;
typedef int GLint;
typedef int GLsizei;
typedef unsigned char GLubyte;
typedef unsigned short GLushort;
typedef unsigned int GLuint;
typedef float GLfloat;
typedef float GLclampf;
typedef double GLdouble;
typedef double GLclampd;
typedef void GLvoid;


// ****************************************************************************
//
// The Callback enabling functionality:
// From Python, callable PyObjects are registered with functions that are named
// the same as their C equivalents in Glut but which are not simple wrappers.
// What they do is store a global pointer to the callable object and register
// a C callback with Glut which knows about the global pointer.
// When Glut calls the callback, it converts it's arguments to Python types and
// evaluates the callable Python object referenced by the global pointer,
// passing it the converted arguments.

%{

/*
 * The Python callbacks.
 */
static PyObject * g_display_func = 0;
static PyObject * g_reshape_func = 0; // Viewport re-shape.
static PyObject * g_keyboard_func = 0;
static PyObject * g_keyboard_up_func = 0;
static PyObject * g_mouse_func = 0;
static PyObject * g_motion_func = 0; // Mouse motion callback.
static PyObject * g_passive_motion_func = 0; // Mouse motion callback.
static PyObject * g_entry_func = 0;
static PyObject * g_visibility_func = 0;
static PyObject * g_idle_func = 0;
static PyObject * g_timer_func = 0;
static PyObject * g_menu_state_func = 0;
static PyObject * g_special_func = 0;
static PyObject * g_spaceball_motion_func = 0;
static PyObject * g_spaceball_rotate_func = 0;
static PyObject * g_spaceball_button_func = 0;
static PyObject * g_button_box_func = 0;
static PyObject * g_dials_func = 0;
static PyObject * g_tablet_motion_func = 0;
static PyObject * g_tablet_button_func = 0;
static PyObject * g_menu_status_func = 0;
static PyObject * g_overlay_display_func = 0;
static PyObject * g_window_status_func = 0;
static PyObject * g_special_up_func = 0;
static PyObject * g_joystick_func = 0;



/*
 * Utilities that build argument lists for Python callbacks and then eval them.
 */

/* Call a PyObject that takes no arguments from the C side.*/
void eval_no_args(PyObject *func){
   PyObject *arglist;
   PyObject *result;

   arglist = Py_BuildValue("()");  // Empty arg list.
   result =  PyEval_CallObject(func, arglist);
   Py_DECREF(arglist);
   Py_XDECREF(result);
   return /*void*/;

}

void eval_1int_arg(PyObject *func, int arg1){
   PyObject *arglist;
   PyObject *result;

   arglist = Py_BuildValue("(i)", arg1);
   result =  PyEval_CallObject(func, arglist);
   Py_DECREF(arglist);
   Py_XDECREF(result);
   return /*void*/;
}

/* Call a PyObject that takes two int arguments.*/
void eval_2int_args(PyObject *func, int arg1, int arg2){
   PyObject *arglist;
   PyObject *result;

   arglist = Py_BuildValue("(ii)", arg1, arg2);
   result =  PyEval_CallObject(func, arglist);
   Py_DECREF(arglist);
   Py_XDECREF(result);
   return /*void*/;
}

void eval_3int_args(PyObject *func, int arg1, int arg2, int arg3){
   PyObject *arglist;
   PyObject *result;

   arglist = Py_BuildValue("(iii)", arg1, arg2, arg3);
   result =  PyEval_CallObject(func, arglist);
   Py_DECREF(arglist);
   Py_XDECREF(result);
   return /*void*/;
}

void eval_4int_args(PyObject *func, int arg1, int arg2, int arg3, int arg4){
   PyObject *arglist;
   PyObject *result;

   arglist = Py_BuildValue("(iiii)", arg1, arg2, arg3, arg4);
   result =  PyEval_CallObject(func, arglist);
   Py_DECREF(arglist);
   Py_XDECREF(result);
   return /*void*/;
}

void eval_1uchar_2int_args(PyObject *func, unsigned char arg1, int arg2, int arg3){
   PyObject *arglist;
   PyObject *result;

   arglist = Py_BuildValue("(iii)", arg1, arg2, arg3);// Promote uchar to int.
   result =  PyEval_CallObject(func, arglist);
   Py_DECREF(arglist);
   Py_XDECREF(result);
   return /*void*/;
}

/* *****************************************************
 * The C functions that Glut sees as it's callbacks.
 * These funcs evaluate the registered Python callbacks.
 */
void evaluate_display_func(void){
   eval_no_args(g_display_func);
}

void evaluate_reshape_func(int x, int y){
    eval_2int_args(g_reshape_func, x, y);
}

void evaluate_keyboard_func(unsigned char key, int x, int y){
    eval_1uchar_2int_args(g_keyboard_func, key, x, y);
}

void evaluate_mouse_func(int button, int state, int x, int y){
    eval_4int_args(g_mouse_func, button, state, x, y);
}
void evaluate_motion_func(int x, int y){
    eval_2int_args(g_motion_func, x, y);
}
void evaluate_passive_motion_func(int x, int y){
    eval_2int_args(g_passive_motion_func, x, y);
}

void evaluate_keyboard_up_func(unsigned char key, int x, int y){
    eval_1uchar_2int_args(g_keyboard_up_func, key, x, y);
}

void evaluate_entry_func(int state){
    eval_1int_arg(g_entry_func, state);
}

void evaluate_visibility_func(int state){
    eval_1int_arg(g_visibility_func, state);
}

void evaluate_idle_func(void){
    eval_no_args(g_idle_func);
}

void evaluate_timer_func(int state){
    eval_1int_arg(g_timer_func, state);
}

void evaluate_menu_state_func(int state){
    eval_1int_arg(g_menu_state_func, state);
}

void evaluate_special_func(int key, int x, int y){
    eval_3int_args(g_special_func, key, x, y);
}

void evaluate_spaceball_motion_func(int x, int y, int z){
    eval_3int_args(g_spaceball_motion_func, x, y, z);
}

void evaluate_spaceball_rotate_func(int x, int y, int z){
    eval_3int_args(g_spaceball_rotate_func, x, y, z);
}

void evaluate_spaceball_button_func(int button, int state){
    eval_2int_args(g_spaceball_button_func, button, state);
}

void evaluate_button_box_func(int button, int state){
    eval_2int_args(g_button_box_func, button, state);
}

void evaluate_dials_func(int dial, int value){
    eval_2int_args(g_dials_func, dial, value);
}

void evaluate_tablet_motion_func(int x, int y){
    eval_2int_args(g_tablet_motion_func, x, y);
}

void evaluate_tablet_button_func(int button, int state, int x, int y){
    eval_4int_args(g_tablet_button_func, button, state, x, y);
}

void evaluate_menu_status_func(int status, int x, int y){
    eval_3int_args(g_menu_status_func, status, x, y);
}

void evaluate_overlay_display_func(void){
    eval_no_args(g_overlay_display_func);
}

void evaluate_window_status_func(int state){
    eval_1int_arg(g_window_status_func, state);
}

void evaluate_special_up_func(int key, int x, int y){
    eval_3int_args(g_special_up_func, key, x, y);
}

void evaluate_joystick_func(unsigned int button_mask, int x, int y, int z){
    eval_4int_args(g_joystick_func, button_mask, x, y, z);
}

// ****************************************************************************
//
// Callback registration functions (Glut API equivalents).
//

#define PY_GLUT_CALLBACK_REG_FUNC_BODY(g_pyfunc, glutFunc, evaluator) \
    PyObject * old = g_pyfunc;\
    g_pyfunc = pyfunc;\
    Py_XINCREF(g_pyfunc);\
    if (!old){\
        /* Register the evaluator func with glut: */\
        glutFunc(evaluator);\
    }\
    Py_XDECREF(old);	        /* Dispose of previous callback */


/* py_glutDisplayFunc()
   Proxy for the glut function which registers a callable Pyobject in the
   static var set aside for it and registers a C-func with glut which calls
   the PyObject through the static var.
 */
void py_glutDisplayFunc(PyObject * pyfunc){
    PY_GLUT_CALLBACK_REG_FUNC_BODY(g_display_func, glutDisplayFunc, evaluate_display_func)
}

void py_glutReshapeFunc(PyObject * pyfunc) {
    PY_GLUT_CALLBACK_REG_FUNC_BODY(g_reshape_func, glutReshapeFunc, evaluate_reshape_func)
}

void py_glutKeyboardFunc(PyObject * pyfunc) {
    PyObject * old = g_keyboard_func;
    g_keyboard_func = pyfunc;
    Py_XINCREF(g_keyboard_func);
    if (!old){
        /* set the actual glut display func. */
        glutKeyboardFunc(evaluate_keyboard_func);
    }
    Py_XDECREF(old);	        /* Dispose of previous callback */
}

void py_glutMouseFunc(PyObject * pyfunc) {
    PyObject * old = g_mouse_func;
    g_mouse_func = pyfunc;
    Py_XINCREF(g_mouse_func);
    if (!old){
        /* set the actual glut display func. */
        glutMouseFunc(evaluate_mouse_func);
    }
    Py_XDECREF(old);	        /* Dispose of previous callback */
}

/* py_glutMotionFunc()
   Register a python function to be evaluated in a callback every time the mouse
   moves with a button held down.
 */
void py_glutMotionFunc(PyObject * pyfunc) {
    PyObject * old = g_motion_func;
    g_motion_func = pyfunc;
    Py_XINCREF(g_motion_func);
    if (!old){
        /* set the actual glut display func. */
        glutMotionFunc(evaluate_motion_func);
    }
    Py_XDECREF(old);	        /* Dispose of previous callback */
}

void py_glutPassiveMotionFunc(PyObject * pyfunc) {
    PyObject * old = g_passive_motion_func;
    g_passive_motion_func = pyfunc;
    Py_XINCREF(g_passive_motion_func);
    if (!old){
        /* set the actual glut display func. */
        glutPassiveMotionFunc(evaluate_passive_motion_func);
    }
    Py_XDECREF(old);	        /* Dispose of previous callback */
}

void py_glutKeyboardUpFunc(PyObject * pyfunc) { 
    PyObject * old = g_keyboard_up_func;
    g_keyboard_up_func = pyfunc;
    Py_XINCREF(g_keyboard_up_func);
    if (!old){
        /* set the actual glut display func. */
        glutKeyboardUpFunc(evaluate_keyboard_up_func);
    }
    Py_XDECREF(old);	        /* Dispose of previous callback */
}

void py_glutEntryFunc(PyObject * pyfunc) { 
    PyObject * old = g_entry_func;
    g_entry_func = pyfunc;
    Py_XINCREF(g_entry_func);
    if (!old){
        /* Register the evaluator func with glut: */
        glutEntryFunc(evaluate_entry_func);
    }
    Py_XDECREF(old);	        /* Dispose of previous callback */
}

void py_glutVisibilityFunc(PyObject * pyfunc) { 
    PY_GLUT_CALLBACK_REG_FUNC_BODY(g_visibility_func, glutVisibilityFunc, evaluate_visibility_func)
}

void py_glutIdleFunc(PyObject * pyfunc) { 
    PY_GLUT_CALLBACK_REG_FUNC_BODY(g_idle_func, glutIdleFunc, evaluate_idle_func)
}

void py_glutTimerFunc(unsigned int millis, PyObject * pyfunc, int value) { 
    PyObject * old = g_timer_func;
    g_timer_func = pyfunc;
    Py_XINCREF(g_timer_func);
    /* Register the evaluator func with glut: */
    glutTimerFunc(millis, evaluate_timer_func, value);
    Py_XDECREF(old);	        /* Dispose of previous callback */
}

void py_glutMenuStateFunc(PyObject * pyfunc){
    PY_GLUT_CALLBACK_REG_FUNC_BODY(g_menu_state_func, glutMenuStateFunc, evaluate_menu_state_func)
}

void py_glutSpecialFunc(PyObject * pyfunc){
    PY_GLUT_CALLBACK_REG_FUNC_BODY(g_special_func, glutSpecialFunc, evaluate_special_func)
}

void py_glutSpaceballMotionFunc(PyObject * pyfunc){
    PY_GLUT_CALLBACK_REG_FUNC_BODY(g_spaceball_motion_func, glutSpaceballMotionFunc, evaluate_spaceball_motion_func)
}

void py_glutSpaceballRotateFunc(PyObject * pyfunc){
    PY_GLUT_CALLBACK_REG_FUNC_BODY(g_spaceball_rotate_func,glutSpaceballRotateFunc, evaluate_spaceball_rotate_func)
}

void py_glutSpaceballButtonFunc(PyObject * pyfunc){
    PY_GLUT_CALLBACK_REG_FUNC_BODY(g_spaceball_button_func, glutSpaceballButtonFunc, evaluate_spaceball_button_func)
}

void py_glutButtonBoxFunc(PyObject * pyfunc){
    PY_GLUT_CALLBACK_REG_FUNC_BODY(g_button_box_func, glutButtonBoxFunc, evaluate_button_box_func)
}

void py_glutDialsFunc(PyObject * pyfunc){
     PY_GLUT_CALLBACK_REG_FUNC_BODY(g_dials_func, glutDialsFunc, evaluate_dials_func)
}

void py_glutTabletMotionFunc(PyObject * pyfunc){
    PY_GLUT_CALLBACK_REG_FUNC_BODY(g_tablet_motion_func, glutTabletMotionFunc, evaluate_tablet_motion_func)
}

void py_glutTabletButtonFunc(PyObject * pyfunc){
    PY_GLUT_CALLBACK_REG_FUNC_BODY(g_tablet_button_func, glutTabletButtonFunc, evaluate_tablet_button_func)
}

void py_glutMenuStatusFunc(PyObject * pyfunc){
    PY_GLUT_CALLBACK_REG_FUNC_BODY(g_menu_status_func, glutMenuStatusFunc, evaluate_menu_status_func)
}

void py_glutOverlayDisplayFunc(PyObject * pyfunc){
    PY_GLUT_CALLBACK_REG_FUNC_BODY(g_overlay_display_func, glutOverlayDisplayFunc, evaluate_overlay_display_func)
}

void py_glutWindowStatusFunc(PyObject * pyfunc){
    PY_GLUT_CALLBACK_REG_FUNC_BODY(g_window_status_func, glutWindowStatusFunc, evaluate_window_status_func)
}

void py_glutSpecialUpFunc(PyObject * pyfunc){
    PY_GLUT_CALLBACK_REG_FUNC_BODY(g_special_up_func, glutSpecialUpFunc, evaluate_special_up_func)
}

void py_glutJoystickFunc(PyObject * pyfunc, int poll_interval){
    PyObject * old = g_joystick_func;
    g_joystick_func = pyfunc;
    Py_XINCREF(g_joystick_func);
    /* Register the evaluator func with glut: */
    glutJoystickFunc(evaluate_joystick_func, poll_interval);
    Py_XDECREF(old);	        /* Dispose of previous callback */
}

// PY_GLUT_CALLBACK_REG_FUNC_BODY(g__func, glutFunc, evaluate__func)

%}


//******************************************************************************
// Check callability of PyObjects passed to extension to be used as callbacks:
//
%typemap(python,in) PyObject *pyfunc {
  if (!PyCallable_Check($source)) {
      PyErr_SetString(PyExc_TypeError, "Need a callable object!");
      return NULL;
  }
  $target = $source;
}

// ****************************************************************************
// General Typemaps:
//

// Glut takes a char ** only in it's glutInit() funtion but this will allocate
// an array and in it make refrences to the internal rep of the elements of a
// Python list of strings for passing to any possible func which takes a char**.
// !!! Works in harmony with the typemap below which frees the memory.
// !!! Doesn't copy the individual strings so called func mustn't keep refrences
// to them around or else the Python caller must store it's original list untill
// it has finished with the extension module.
%typemap(python,in) char ** {
	
  /* Check if is a list */
  if (PyList_Check($source)) {
    int size = PyList_Size($source);
    int i = 0;
    $target = (char **) malloc((size+1)*sizeof(char *));
    for (i = 0; i < size; i++) {
      PyObject *o = PyList_GetItem($source,i);
      if (PyString_Check(o))
	$target[i] = PyString_AsString(PyList_GetItem($source,i));
      else {
	PyErr_SetString(PyExc_TypeError,"list must contain strings");
	free($target);
	return NULL;
      }
    }
    $target[i] = 0;
  } else {
    PyErr_SetString(PyExc_TypeError,"not a list");
    return NULL;
  }
}

// Cleanup the char ** allocated to hold argv passed to glutInit().
%typemap(python,freearg) char ** {
  free((char *) $source);
}

// Check that a function with a parm called argcp recieves an integer value:
%typemap(python, in) int* argcp {
    if (PyInt_Check($source)) {
        int argc = PyInt_AS_LONG($source);
        $target = &argc;
    } else {
        PyErr_SetString(PyExc_TypeError,"not an int");
        return NULL;
    }
}

// ****************************************************************************
// ****************************************************************************
// Wrapped Functions:


// Callback registration:

/* GLUT window callback sub-API. */
%name(glutDisplayFunc)		void py_glutDisplayFunc(PyObject * pyfunc);
%name(glutReshapeFunc)		void py_glutReshapeFunc(PyObject * pyfunc);
%name(glutKeyboardFunc)		void py_glutKeyboardFunc(PyObject * pyfunc);
%name(glutMouseFunc)		void py_glutMouseFunc(PyObject * pyfunc);
%name(glutMotionFunc)		void py_glutMotionFunc(PyObject * pyfunc);
%name(glutPassiveMotionFunc) void py_glutPassiveMotionFunc(PyObject * pyfunc);
%name(glutEntryFunc)		void py_glutEntryFunc(PyObject * pyfunc);
%name(glutVisibilityFunc)	void py_glutVisibilityFunc(PyObject * pyfunc);
%name(glutIdleFunc)			void py_glutIdleFunc(PyObject * pyfunc);
%name(glutTimerFunc)        void py_glutTimerFunc(unsigned int millis, PyObject * pyfunc, int state);
%name(glutMenuStateFunc)    void py_glutMenuStateFunc(PyObject * pyfunc);
//#if (GLUT_API_VERSION >= 2)
%name(glutSpecialFunc)      void py_glutSpecialFunc(PyObject * pyfunc);
%name(glutSpaceballMotionFunc) void py_glutSpaceballMotionFunc(PyObject * pyfunc);
%name(glutSpaceballRotateFunc) void py_glutSpaceballRotateFunc(PyObject * pyfunc);
%name(glutSpaceballButtonFunc) void py_glutSpaceballButtonFunc(PyObject * pyfunc);
%name(glutButtonBoxFunc)    void py_glutButtonBoxFunc(PyObject * pyfunc);
%name(glutDialsFunc)        void py_glutDialsFunc(PyObject * pyfunc);
%name(glutTabletMotionFunc) void py_glutTabletMotionFunc(PyObject * pyfunc);
%name(glutTabletButtonFunc) void py_glutTabletButtonFunc(PyObject * pyfunc);
//#if (GLUT_API_VERSION >= 3)
%name(glutMenuStatusFunc)   void py_glutMenuStatusFunc(PyObject * pyfunc);
%name(glutOverlayDisplayFunc) void py_glutOverlayDisplayFunc(PyObject * pyfunc);
//#if (GLUT_API_VERSION >= 4 || GLUT_XLIB_IMPLEMENTATION >= 9)
%name(glutWindowStatusFunc) void py_glutWindowStatusFunc(PyObject * pyfunc);
//#endif
//#if (GLUT_API_VERSION >= 4 || GLUT_XLIB_IMPLEMENTATION >= 13)
%name(glutKeyboardUpFunc) void py_glutKeyboardUpFunc(PyObject * pyfunc);
%name(glutSpecialUpFunc)  void py_glutSpecialUpFunc(PyObject * pyfunc);
%name(glutJoystickFunc)   void py_glutJoystickFunc(PyObject * pyfunc, int poll_interval);
//#endif
//#endif
//#endif

/* GLUT initialization sub-API. */

void glutInit(int *argcp, char **argv);
void glutInitDisplayMode(unsigned int mode);
//#if (GLUT_API_VERSION >= 4 || GLUT_XLIB_IMPLEMENTATION >= 9)
 void  glutInitDisplayString(const char *string);
//#endif
void glutInitWindowPosition(int x, int y);
void glutInitWindowSize(int width, int height);
void glutMainLoop(void);

/* GLUT window sub-API. */

int glutCreateWindow(const char *title);
int glutCreateSubWindow(int win, int x, int y, int width, int height);
void glutDestroyWindow(int win);
void glutPostRedisplay(void);
//#if (GLUT_API_VERSION >= 4 || GLUT_XLIB_IMPLEMENTATION >= 11)
 void  glutPostWindowRedisplay(int win);
//#endif

void glutSwapBuffers(void);
int  glutGetWindow(void);
void glutSetWindow(int win);
void glutSetWindowTitle(const char *title);
void glutSetIconTitle(const char *title);
void glutPositionWindow(int x, int y);
void glutReshapeWindow(int width, int height);
void glutPopWindow(void);
void glutPushWindow(void);
void glutIconifyWindow(void);
void glutShowWindow(void);
void glutHideWindow(void);
//#if (GLUT_API_VERSION >= 3)
 void  glutFullScreen(void);
 void  glutSetCursor(int cursor);
//#if (GLUT_API_VERSION >= 4 || GLUT_XLIB_IMPLEMENTATION >= 9)
 void  glutWarpPointer(int x, int y);
//#endif

/* GLUT overlay sub-API. */
 void  glutEstablishOverlay(void);
 void  glutRemoveOverlay(void);
 void  glutUseLayer(GLenum layer);
 void  glutPostOverlayRedisplay(void);
//#if (GLUT_API_VERSION >= 4 || GLUT_XLIB_IMPLEMENTATION >= 11)
 void  glutPostWindowOverlayRedisplay(int win);
//#endif
 void  glutShowOverlay(void);
 void  glutHideOverlay(void);
//#endif

/* GLUT menu sub-API. */
%{
#define PY_GLUT_NUM_MENU_FUNCS 64

	static PyObject * g_menu_funcs[PY_GLUT_NUM_MENU_FUNCS] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

	void pyglut_menu_func(int event){
		int menu_index = glutGetMenu() - 1;
		PyObject* py_func = g_menu_funcs[menu_index];

		eval_1int_arg(py_func, event);
	}

	int py_glutCreateMenu(PyObject * pyfunc){
		int menu = glutCreateMenu(pyglut_menu_func);
		if(menu > PY_GLUT_NUM_MENU_FUNCS) { // ! menu is 1 based not zero based.
			glutDestroyMenu(menu);
			PyErr_SetString(PyExc_RuntimeError,"PyGLUT has no more space for menus.");			
			return 0; // Does this make sense given there is an outer wrapper func? (FixMe)
		}
		g_menu_funcs[menu - 1] = pyfunc;
		Py_XINCREF(pyfunc); // Fragile !!!
		return menu;
	}

	void py_glutDestroyMenu(int menu){
		Py_XDECREF(g_menu_funcs[menu - 1]);
		g_menu_funcs[menu - 1] = NULL;
		glutDestroyMenu(menu);
	}
%}
 %name(glutCreateMenu) int py_glutCreateMenu(PyObject * pyfunc);
 %name (glutDestroyMenu) void  py_glutDestroyMenu(int menu);
 int  glutGetMenu(void);
 void  glutSetMenu(int menu);
 void  glutAddMenuEntry(const char *label, int value);
 void  glutAddSubMenu(const char *label, int submenu);
 void  glutChangeToMenuEntry(int item, const char *label, int value);
 void  glutChangeToSubMenu(int item, const char *label, int submenu);
 void  glutRemoveMenuItem(int item);
 void  glutAttachMenu(int button);
 void  glutDetachMenu(int button);

/* GLUT color index sub-API. */
 void  glutSetColor(int, GLfloat red, GLfloat green, GLfloat blue);
 GLfloat  glutGetColor(int ndx, int component);
 void  glutCopyColormap(int win);

/* GLUT state retrieval sub-API. */
 int  glutGet(GLenum type);
 int  glutDeviceGet(GLenum type);

//#if (GLUT_API_VERSION >= 2)
/* GLUT extension support sub-API */
 int  glutExtensionSupported(const char *name);
//#endif
//#if (GLUT_API_VERSION >= 3)
 int  glutGetModifiers(void);
 int  glutLayerGet(GLenum type);
//#endif

/* GLUT font sub-API */
%{
// On MS Windows fonts are ints disguised as void*s. On Unix they realy are
// void*s.
// On the Python side fonts are known by int values.

#define PY_GLUT_MAX_FONT 8

// font_from_int()
// This util func translates those ints to an appropriate void* for passing
// into GLUT:
void* font_from_int(int py_font){
	switch(py_font){
		case 0:
		return GLUT_STROKE_ROMAN;
		case 1:
		return GLUT_STROKE_MONO_ROMAN;
		case 2:
		return GLUT_BITMAP_9_BY_15;
		case 3:
		return GLUT_BITMAP_8_BY_13;
		case 4:
		return GLUT_BITMAP_TIMES_ROMAN_10;
		case 5:
		return GLUT_BITMAP_TIMES_ROMAN_24;
#	if (GLUT_API_VERSION >= 3)
		case 6:
		return GLUT_BITMAP_HELVETICA_10;
		case 7:
		return GLUT_BITMAP_HELVETICA_12;
		case 8:
		return GLUT_BITMAP_HELVETICA_18;
#	endif
	};

	// We can never get here thanks to the typemap that checks arguments
	// caled 'font' so we could return anything....hmmm
	return (void*) 0xcccccccc;
}

void py_glutBitmapCharacter(int font, int character){
	glutBitmapCharacter(font_from_int(font), character);
}

int  py_glutBitmapWidth(int font, int character){
	return glutBitmapWidth(font_from_int(font), character);
}

void  py_glutStrokeCharacter(int font, int character){
	glutStrokeCharacter(font_from_int(font), character);
}

int  py_glutStrokeWidth(int font, int character){
	return glutStrokeWidth(font_from_int(font), character);
}

//#if (GLUT_API_VERSION >= 4 || GLUT_XLIB_IMPLEMENTATION >= 9)
int  py_glutBitmapLength(int font, const unsigned char *string){
	return glutBitmapLength(font_from_int(font), string);
}

int  py_glutStrokeLength(int font, const unsigned char *string){
	return glutStrokeLength(font_from_int(font), string);
}
//#endif

%}

// Check that a function with an arg called font recieves an integer value that
// is a valid font identifier:
%typemap(python, in) int font {
    if (PyInt_Check($source)) {
        int font = PyInt_AS_LONG($source);
		if(font > PY_GLUT_MAX_FONT || font < 0){
			PyErr_SetString(PyExc_ValueError,"Argument not in range for a font ID as of GLUT_XLIB_IMPLEMENTATION=13.");
			return NULL;
		} else {
			$target = font;
		}
    } else {
        PyErr_SetString(PyExc_TypeError,"font ID should be an int.");
        return NULL;
    }
}

%name(glutBitmapCharacter) void py_glutBitmapCharacter(int font, int character);
%name(glutBitmapWidth) int  py_glutBitmapWidth(int font, int character);
%name(glutStrokeCharacter) void  py_glutStrokeCharacter(int font, int character);
%name(glutStrokeWidth) int  py_glutStrokeWidth(int font, int character);
//#if (GLUT_API_VERSION >= 4 || GLUT_XLIB_IMPLEMENTATION >= 9)
%name(glutBitmapLength) int  py_glutBitmapLength(int font, const unsigned char *string);
%name(glutStrokeLength) int  py_glutStrokeLength(int font, const unsigned char *string);
//#endif

/* GLUT pre-built models sub-API */

void glutWireSphere(GLdouble radius, GLint slices, GLint stacks);
void glutSolidSphere(GLdouble radius, GLint slices, GLint stacks);
void glutWireCone(GLdouble base, GLdouble height, GLint slices, GLint stacks);
void glutSolidCone(GLdouble base, GLdouble height, GLint slices, GLint stacks);
void glutWireCube(GLdouble size);
void  glutSolidCube(GLdouble size);
void  glutWireTorus(GLdouble innerRadius, GLdouble outerRadius, GLint sides, GLint rings);
void  glutSolidTorus(GLdouble innerRadius, GLdouble outerRadius, GLint sides, GLint rings);
void  glutWireDodecahedron(void);
void  glutSolidDodecahedron(void);
void  glutWireTeapot(GLdouble size);
void  glutSolidTeapot(GLdouble size);
void  glutWireOctahedron(void);
void  glutSolidOctahedron(void);
void  glutWireTetrahedron(void);
void  glutSolidTetrahedron(void);
void  glutWireIcosahedron(void);
void  glutSolidIcosahedron(void);

//#if (GLUT_API_VERSION >= 4 || GLUT_XLIB_IMPLEMENTATION >= 9)
/* GLUT video resize sub-API. */
 int  glutVideoResizeGet(GLenum param);
 void  glutSetupVideoResizing(void);
 void  glutStopVideoResizing(void);
 void  glutVideoResize(int x, int y, int width, int height);
 void  glutVideoPan(int x, int y, int width, int height);

/* GLUT debugging sub-API. */
 void  glutReportErrors(void);
//#endif

// #if (GLUT_API_VERSION >= 4 || GLUT_XLIB_IMPLEMENTATION >= 13) // [AHC]
/* GLUT device control sub-API. */
/* glutSetKeyRepeat modes. */
#define GLUT_KEY_REPEAT_OFF		0
#define GLUT_KEY_REPEAT_ON		1
#define GLUT_KEY_REPEAT_DEFAULT		2

/* Joystick button masks. */
#define GLUT_JOYSTICK_BUTTON_A		1
#define GLUT_JOYSTICK_BUTTON_B		2
#define GLUT_JOYSTICK_BUTTON_C		4
#define GLUT_JOYSTICK_BUTTON_D		8

 void  glutIgnoreKeyRepeat(int ignore);
 void  glutSetKeyRepeat(int repeatMode);
 void  glutForceJoystickFunc(void);

/* GLUT game mode sub-API. */
/* glutGameModeGet. */
#define GLUT_GAME_MODE_ACTIVE           0
#define GLUT_GAME_MODE_POSSIBLE         1
#define GLUT_GAME_MODE_WIDTH            2
#define GLUT_GAME_MODE_HEIGHT           3
#define GLUT_GAME_MODE_PIXEL_DEPTH      4
#define GLUT_GAME_MODE_REFRESH_RATE     5
#define GLUT_GAME_MODE_DISPLAY_CHANGED  6

 void  glutGameModeString(const char *string);
 int  glutEnterGameMode(void);
 void  glutLeaveGameMode(void);
 int  glutGameModeGet(GLenum mode);
// #endif // [AHC]

