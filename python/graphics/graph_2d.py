from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *

X = 1.0
Y = 1.0
J = 0.0
K = 1.0
M = 1.0
U = 0.0
point = 100
ANGLE = 12.6 * M / point
    
def resize(width, height):
    ar = width / height
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(100, ar, 1, 5)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

def display():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()
    gluLookAt (0, 0, 5, 0, 0, 0, 0, 1, 0)

    for i in range(-point, point):
        import math
        t = ANGLE * i
        x = K * (t + J)
        glPointSize(2)
        glBegin(GL_POINTS)
        glColor3f(1, 0, 0)
        glVertex2f(X * t, Y*math.sin(x) + U)
        glColor3f(1, 0, 0)
        glVertex2f(X * t, Y * U)
        glEnd()

        glPointSize(2)
        glBegin(GL_POINTS)
        glColor3f(0, 0, 0)
        glVertex2f(X * t, Y * U)
        glVertex2f(-X * J, Y * t)
        glEnd()
    glutSwapBuffers()

def key(*args):
    if args[0] == 'q': exit(0)
    glutPostRedisplay()

def idle():
    glutPostRedisplay()

if __name__ == "__main__":
    import sys
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH)
    glutInitWindowSize(1080, 550)
    glutCreateWindow(sys.argv[0])
    glutReshapeFunc(resize)
    glutDisplayFunc(display)
    glutKeyboardFunc(key)
    glutIdleFunc(idle)
    glClearColor(1, 1, 1, 1)
    glutMainLoop()
