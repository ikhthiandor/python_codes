from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *

lim, x1, angle = 4, 0.25, -65

def z(x, y):
    import math
    return 2*math.sin(x)*math.cos(y)

def resize(width, height):
    ar = width / height
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(125, ar, 1, 100)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

def display():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()
    gluLookAt (0, 0, 5, 0, 0, 0, 0, 1, 0)
    
    t = glutGet(GLUT_ELAPSED_TIME) / 1000.0
    a = t * 10
    glRotatef(angle, 1, 0, 0)
    glRotatef(a, 0, 0, 1)

    x = -lim
    while x <= lim:
        y = -lim
        while y <= lim:
            glPointSize(2)
            glBegin(GL_POINTS)
            glColor3f(0, 0.5, 1)
            x += x1
            glVertex3f(x, y, z(x, y))
            x -= x1
            glVertex3f(x, y, z(x, y))
            glEnd()
            y += x1
        x += x1
    glutSwapBuffers()
    
def idle():
    glutPostRedisplay()

import sys
glutInit(sys.argv)
glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE)
glutInitWindowSize(1080, 550)
glutCreateWindow(sys.argv[0])
glutReshapeFunc(resize)
glutDisplayFunc(display)
glutIdleFunc(idle)
glClearColor(0, 0, 0, 1)
glutMainLoop()
