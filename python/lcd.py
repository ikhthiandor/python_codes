global zoom

def func1():
    print ' ',
    for i in range(zoom): print ' ',
    print '  ',

def func2():
    print ' ',
    for i in range(zoom): print '+',
    print '  ',

def func3():
    print '|',
    for i in range(zoom): print ' ',
    print '  ',

def func4():
    print ' ',
    for i in range(zoom): print ' ',
    print '| ',

def func5():
    print '|',
    for i in range(zoom): print ' ',
    print '| ',
    
while 1:
    num = input('enter an integer:\t')
    zoom = input('enter the zoom level:\t') 
    arr = '%s' % num
    digit = len(arr)
    print '\n',

    for i in range(digit):
        if arr[i] == '1' or arr[i] == '4': func1()
        else : func2()
    print '\n',

    for j in range(zoom):
        for i in range(digit):
            if arr[i] == '5' or arr[i] == '6': func3()
            elif arr[i] == '1' or arr[i] == '2' or arr[i] == '3' \
                 or arr[i] == '7': func4()
            else :func5()
        print '\n',

    for i in range(digit):
        if arr[i] == '0' or arr[i] == '1' or arr[i] == '7': func1()
        else: func2()
    print '\n',

    for j in range(zoom):
        for i in range(digit):
            if arr[i] == '2': func3()
            elif arr[i] == '0' or arr[i] == '6' or arr[i] == '8': func5()
            else: func4()
        print '\n',

    for i in range(digit):
        if arr[i] == '4' or arr[i] == '1' or arr[i] == '7': func1()
        else: func2()
    print '\n',
