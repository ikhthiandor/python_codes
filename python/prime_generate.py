while 1:
    num = input('enter an integer:\t')
    sqrt = int(num ** 0.5) + 1
    number = [1 for x in xrange(num)]
    for pos in xrange(3, sqrt, 2):
        if number[pos]:
            for square_pos in xrange(pos ** 2, num, pos << 1):
               number[square_pos] = 0
    i = 1
    print '%10d' % 2,
    for pos in xrange(3, num, 2):
        if number[pos]:
            i += 1
            print '%10d' % pos,
            if not i % 10: print '\n',
    print '\n'
