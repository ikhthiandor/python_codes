while 1:
    RANGE = input('\nenter an integer:\t')
    d = input('enter no. of prime in row:\t')
    number = [1 for i in range(RANGE + 1)]
    number[1] = 0
    for i in range(3, int(RANGE ** 0.5) + 1, 2):
        if number[i]:
            for j in range(i*i, RANGE + 1, i<<1): number[j] = 0
    for i in range(1, RANGE + 1, 2):
        if i % d == 1: print '\n%08d' % i,
        if number[i]: print '.',
        else: print ' ',
