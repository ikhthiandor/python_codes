#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     21-02-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

# this code extracts a link from webpage


page ='<div id="top_bin"><div id="top_content" class="width960"><div class="udacity float-left"><a href="http://www.xkcd.com">'

start_link = page.find("<a href=")




quote_starts = page.find('"',start_link)



quote_ends = page.find('"',quote_starts+1)




url = page[quote_starts+1 : quote_ends]

print(url)
