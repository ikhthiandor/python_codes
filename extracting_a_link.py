#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     21-02-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

# this code extracts a link from webpage


page ='<div id="top_bin"><div id="top_content" class="width960"><div class="udacity float-left"><a href="http://www.xkcd.com">'

start_link = page.find("<a href=")

anchor_tag = page[start_link:]

# print(anchor_tag)

# now we have to find where the quote starts inside the anchor tag

quote_starts = anchor_tag.find('"')



http_part = anchor_tag[quote_starts+1 : ]

print(http_part)

# now we need to find the index where the quote ends within http_part

quote_ends = http_part.find('"')

print(quote_ends)
url = http_part[:quote_ends]

print(url)
