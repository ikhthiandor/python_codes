#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     28-02-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

sequence = [2,7,1,9,1,3]

def bubblesort(theSeq):
    n = len(theSeq)

    for i in range(n -1):
        for j in range(i + n -1):
            if theSeq[j] > theSeq[j+1]:
                tmp = theSeq[j]
                theSeq[j] = theSeq[j+1]
                theSeq[j+1] = tmp

    return theSeq


bubblesort(sequence)

print (sequence)


