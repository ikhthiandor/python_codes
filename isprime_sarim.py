#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     09-11-2011
# Copyright:   (c) Majibur Rahman 2011
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python



num = 23432953958
i = 3

isprime = 1
if num % 2 == 0:
    isprime = 0
else:
    while i<num:
        if num % i == 0:
            isprime = 0
            break
        i=i+2

if isprime == 1:
    print "It is prime"
else:
    print "It is not prime"
