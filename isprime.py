#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     05-11-2011
# Copyright:   (c) Majibur Rahman 2011
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

num = 7
i = 2

isprime = 1
while i<num:
    if num % i == 0:
        isprime = 0
        break
    i = i + 1

if isprime == 1:
    print "It is prime"
else:
    print "It is not prime"






