#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     19-04-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python


# Tricky REs with ^ and \

# Assign to regexp a regular expression for double-quoted string literals that
# allows for escaped double quotes.

# Hint: Escape " and \
# Hint: (?: (?: ) )

import re

regexp = r'"(?:[^\\"]|(?:\\.))*"'

# regexp matches:

print re.findall(regexp,'"I say, \\"hello.\\""') == ['"I say, \\"hello.\\""']
#>>> True


# regexp does not match:

print re.findall(regexp,'"\\"') != ['"\\"']
#>>> True

