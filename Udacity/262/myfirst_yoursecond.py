#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     17-04-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

# Selecting Substrings : Writing a Python Procedure

# Let p and q each be strings containing two words separated by a space.

# Examples:
#    "bell hooks"
#    "grace hopper"
#    "alonzo church"

# Write a procedure called myfirst_yoursecond(p,q) that returns True if the
# first word in p equals the second word in q.

def myfirst_yoursecond(p,q):

    s1_space_pos = p.find(" ")
    first = p[ : s1_space_pos]

    s2_space_pos = q.find(" ")
    second = q[ s2_space_pos+1 : ]

    if first == second:
        return True
    return False








#print myfirst_yoursecond("bell hooks", "curer bell")
#>>> True

