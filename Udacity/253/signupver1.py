#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2


form = """
<html>
  <head>
    <title>Sign Up</title>
    <style type="text/css">
      .label {text-align: right}
      .error {color: red}
    </style>

  </head>

  <body>
    <h2>Signup</h2>
    <form method="post">
      <table>
        <tr>
          <td class="label">
            Username
          </td>
          <td>
            <input type="text" name="username" value="%(username)s">
          </td>
          <td class="error" name="username_error" >
            %(username_error)s
          </td>
        </tr>

        <tr>
          <td class="label">
            Password
          </td>
          <td>
            <input type="password" name="password" value="">
          </td>
          <td class="error">
            %(pass_error)s
          </td>
        </tr>

        <tr>
          <td class="label">
            Verify Password
          </td>
          <td>
            <input type="password" name="verify" value="">
          </td>
          <td class="error">
            %(pass_match_error)s
          </td>
        </tr>

        <tr>
          <td class="label">
            Email (optional)
          </td>
          <td>
            <input type="text" name="email" value="%(email)s">
          </td>
          <td class="error">
          %(email_error)s

          </td>
        </tr>
      </table>

      <input type="submit">
    </form>
  </body>

</html>

"""

import re


def valid_username(username):

    USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
    return USER_RE.match(username)

def valid_password(password):


    USER_RE = re.compile("^.{3,20}$")
    return USER_RE.match(password)

def verify_password(s1,s2):
    return s1 == s2

def valid_email(user_email):
    if len(user_email) == 0:
        return True
    else:
      USER_RE = re.compile(r"^[\S]+@[\S]+\.[\S]+$")
      return USER_RE.match(user_email)

user_info = {
    "username" :"",
"password1": "",
"password2": "",
"email": ""
}

error_msgs = {
                "username_error": "",
                "pass_error": "",
                "pass_match_error":"",
                "email_error": ""
                                        }

class MainHandler(webapp2.RequestHandler):


    def write_form(self, error_msgs, user_info):
        self.response.out.write(form % {"username": user_info["username"],
                                        "password1": user_info["password1"],
                                        "password2": user_info["password2"],
                                        "email": user_info["email"],

                                        "username_error": error_msgs["username_error"],
                                        "pass_error": error_msgs["pass_error"],
                                        "pass_match_error": error_msgs["pass_match_error"],
                                        "email_error": error_msgs["email_error"]
                                        })

    def get(self):
        self.write_form(error_msgs, user_info)

    def post(self):
        user_name = self.request.get('username')
        password1 = self.request.get('password')
        password2 = self.request.get('verify')
        user_email =  self.request.get('email')

        user_info["username"] = user_name
        user_info["password1"] = password1
        user_info["password2"] = password2
        user_info["email"] = user_email

        username_ok = valid_username(user_name)
        password1_ok = valid_password(password1)
##        password2_ok = valid_password(password2)
        pass_match_ok = verify_password(password1,password2)
        email_ok = valid_email(user_email)

        username_error = "That's not a valid username."
        pass_error = "That's not a valid password."
        pass_match_error = "Passwords don't match"
        email_error = "That's not a valid email"



        if (username_ok and password1_ok and pass_match_ok and email_ok):
            self.redirect('/welcome')
        else:
            if not username_ok:

                error_msgs["username_error"] = username_error
            else:
                error_msgs["username_error"] = ""
            if not pass_match_ok:
                error_msgs["pass_match_error"] = pass_match_error

            else:
                if not password1_ok:
                    user_info["password1"] = ""
                    user_info["password2"] = ""
                    error_msgs["pass_error"] = pass_error

##            if not password2_ok:
##                user_info["password1"] = ""
##                user_info["password2"] = ""


            if not email_ok:

                error_msgs["email_error"] = email_error
            else:
                error_msgs["email_error"] = ""
            self.write_form(error_msgs,user_info)



class WelcomeHandler(webapp2.RequestHandler):
    def get(self):

        username = self.request.get('username')
        self.response.out.write("Welcome, " + username)




app = webapp2.WSGIApplication([('/', MainHandler), ('/welcome', WelcomeHandler)],
                              debug=True)
