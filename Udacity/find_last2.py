#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     04-03-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

# Define a procedure, find_last, that takes as input
# two strings, a search string and a target string,
# and outputs the last position in the search string
# where the target string appears, or -1 if there
# are no occurences.
#
# Example: find_last('aaaa', 'a') returns 3

# Make sure your procedure has a return statement.

def find_last(search, target):
    first_query = search.find(target)

    if(first_query == -1):
        return -1

    else:
        pos = first_query + 1
        while(True):
            ret_value = search.find(target,pos)

            if(ret_value != -1):
                pos = ret_value + 1

            else:
                return pos - 1
                break


# print find_last('mac','ui')















