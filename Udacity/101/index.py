#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     14-03-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

#Define a procedure, add_to_index,
#that takes 3 inputs:

# - an index: [[<keyword>,[<url>,...]],...]
# - a keyword: String
# - a url: String

#If the keyword is already
#in the index, add the url
#to the list of urls associated
#with that keyword.

#If the keyword is not in the index,
#add an entry to the index: [keyword,[url]]

index = []


#add_to_index(index,'udacity','http://udacity.com')
#add_to_index(index,'computing','http://acm.org')
#add_to_index(index,'udacity','http://npr.org')
#print index => [['udacity', ['http://udacity.com', 'http://npr.org']], ['computing', ['http://acm.org']]]

def add_to_index(index,keyword,url):
    if len(index) == 0:
        temp1 = []
        url_list1 = []
        temp1.append(keyword)
        url_list1.append(url)
        temp1.append(url_list1)
        index.append(temp1)

    else:
        for e in index:
            if(e[0] == keyword):

                if url not in e[1]:
                    e[1].append(url)

                return

        temp2 = []
        url_list2 = []
        temp2.append(keyword)
        url_list2.append(url)
        temp2.append(url_list2)
        index.append(temp2)

add_to_index(index,'udacity','http://udacity.com')
add_to_index(index,'computing','http://acm.org')
add_to_index(index,'udacity','http://udacity.com')

print index
