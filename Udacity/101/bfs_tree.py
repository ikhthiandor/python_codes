#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     13-03-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     13-03-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

#The web crawler we built at the
#end of Unit 2 has some serious
#flaws if we were going to use
#it in a real crawler. One
#problem is if we start with
#a good seed page, it might
#run for an extremely long
#time (even forever, since the
#number of URLS on the web is not
#actually finite). The final two
#questions of the homework ask
#you to explore two different ways
#to limit the pages that it can
#crawl.


#######

#TWO GOLD STARS#

#Modify the crawl_web procedure
#to take a second parameter,
#max_depth, that limits the
#minimum number of consecutive
#links that would need to be followed
#from the seed page to reach this
#page. For example, if max_depth
#is 0, the only page that should
#be crawled is the seed page.
#If max_depth is 1, the pages
#that should be crawled are the
#seed page and every page that links
#to it directly. If max_depth is 2,
#the crawl should also include all pages
#that are linked to by these pages.


#The following definition of
#get_page provides an interface
#to the website found at
#http://www.udacity.com/cs101x/index.html

#The function output order does not affect grading.

#crawl_web("http://www.udacity.com/cs101x/index.html",0) => ['http://www.udacity.com/cs101x/index.html']
#crawl_web("http://www.udacity.com/cs101x/index.html",1) => ['http://www.udacity.com/cs101x/index.html', 'http://www.udacity.com/cs101x/flying.html', 'http://www.udacity.com/cs101x/walking.html', 'http://www.udacity.com/cs101x/crawling.html']
#crawl_web("http://www.udacity.com/cs101x/index.html",50) => ['http://www.udacity.com/cs101x/index.html', 'http://www.udacity.com/cs101x/flying.html', 'http://www.udacity.com/cs101x/walking.html', 'http://www.udacity.com/cs101x/crawling.html', 'http://www.udacity.com/cs101x/kicking.html']


##def get_page(url):
##    try:
##        if url == "http://www.udacity.com/cs101x/index.html":
##            return  '<html> <body> This is a test page for learning to crawl! <p> It is a good idea to  <a href="http://www.udacity.com/cs101x/crawling.html">learn to crawl</a> before you try to  <a href="http://www.udacity.com/cs101x/walking.html">walk</a> or  <a href="http://www.udacity.com/cs101x/flying.html">fly</a>. </p> </body> </html> '
##        elif url == "A":
##            return '<a href="B">B</a> <a href="C">C</a><a href="D">D</a>'
##        elif url == "B":
##            return '<a href="H">H</a> <a href="I">I</a>'
##        elif url == "C":
##            return '<a href="G">G</a>'
##        elif url == "D":
##            return '<a href="E">E</a> <a href="F">F</a>'
##        elif url == "E":
##            return '<a href="M">M</a>'
##        elif url == "F":
##            return 'emptiness'
##        elif url == "G":
##            return 'emptiness'
##        elif url == "H":
##            return 'emptiness'
##        elif url == "I":
##            return '<a href="J">J</a> <a href="K">K</a>'
##        elif url == "J":
##            return 'nothing'
##        elif url == "K":
##            return '<a href="L">L</a>'
##        elif url == "L":
##            return 'nothing'
##        elif url == "M":
##            return 'nothing'
##    except:
##        return ""
##    return ""

def get_page(url):
    try:
        if url == "http://www.udacity.com/cs101x/index.html":
            return  '<html> <body> This is a test page for learning to crawl! <p> It is a good idea to  <a href="http://www.udacity.com/cs101x/crawling.html">learn to crawl</a> before you try to  <a href="http://www.udacity.com/cs101x/walking.html">walk</a> or  <a href="http://www.udacity.com/cs101x/flying.html">fly</a>. </p> </body> </html> '
        elif url == "http://www.udacity.com/cs101x/crawling.html":
            return  '<html> <body> I have not learned to crawl yet, but I am quite good at  <a href="http://www.udacity.com/cs101x/kicking.html">kicking</a>. </body> </html>'
        elif url == "http://www.udacity.com/cs101x/walking.html":
            return '<html> <body> I cant get enough  <a href="http://www.udacity.com/cs101x/index.html">crawling</a>! </body> </html>'
        elif url == "http://www.udacity.com/cs101x/flying.html":
            return '<html> <body> The magic words are Squeamish Ossifrage! </body> </html>'
    except:
        return ""
    return ""

def get_next_target(page):
    start_link = page.find('<a href=')
    if start_link == -1:
        return None, 0
    start_quote = page.find('"', start_link)
    end_quote = page.find('"', start_quote + 1)
    url = page[start_quote + 1:end_quote]
    return url, end_quote

def union(p,q):
    for e in q:
        if e not in p:
            p.append(e)


def get_all_links(page):
    links = []
    while True:
        url,endpos = get_next_target(page)
        if url:
            links.append(url)
            page = page[endpos:]
        else:
            break
    return links


def not_in_crawled(crawled, ls):
    result = []
    for e in ls:
        if e not in crawled:
            result.append(e)
    return result



def crawl_web(seed,max_depth):
    tocrawl = [seed]
    crawled = []
    childs = []
    depth = 0
    while tocrawl:

        page = tocrawl.pop()

        if page not in crawled:

            crawled.append(page)
            all_links = get_all_links(get_page(page))
            childs += not_in_crawled(crawled, all_links)

        if(len(tocrawl) == 0):
           if (depth < max_depth):
            tocrawl = childs
            childs = []
            depth = depth + 1


    return crawled









print crawl_web("http://www.udacity.com/cs101x/index.html",3)


