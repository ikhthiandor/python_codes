#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     24-03-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

def make_hashtable(nbuckets):
    hashtable = []
    for i in range(nbuckets):
        hashtable.append([])

    return hashtable

print(make_hashtable(5))