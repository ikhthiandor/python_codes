#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     29-02-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

#Define a procedure, find_second, that takes
#two strings as its inputs: a search string
#and a target string. It should output a
#number that is the position of the second
#occurence of the target string in the
#search string.

#danton = "De l'audace, encore de l'audace, toujours de l'audace"
#print find_second(danton, 'audace') => 25

def find_second(search,target):
    first_occurance = search.find(target)

    second_occurance = search.find(target,first_occurance + 1)

    return second_occurance

danton = "De l'audace, encore de l'audace, toujours de l'audace"

print find_second(danton, "audace")

