#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     14-03-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

def lookup(index,keyword):

    for entry in index:
        if(entry[0] == keyword):
            return entry[1]

    return []