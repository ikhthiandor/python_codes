#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     12-03-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

#Define a procedure, total_enrollment,
#that takes as an input a list of elements,
#where each element is a list containing
#three elements: a university name,
#the total number of students enrollect,
#and the annual tuition.

#The procedure should return two numbers,
#not a string,
#giving the total number of students
#enrolled at all of the universities
#in the list, and the total tuition
#(which is the sum of the number
#of students enrolled times the
#tuition for each university).


usa_univs = [ ['California Institute of Technology',2175,37704],
              ['Harvard',19627,39849],
              ['Massachusetts Institute of Technology',10566,40732],
              ['Princeton',7802,37000],
              ['Rice',5879,35551],
              ['Stanford',19535,40569],
              ['Yale',11701,40500]  ]

total_stu = 0
total_tuition = 0
for e in usa_univs:
    total_stu += e[1]
    #print e[1]

    product = e[1] * e[2]
    total_tuition += product


print total_stu
print total_tuition