#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     05-03-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

#Given the variable,

days_in_month = [31,28,31,30,31,30,31,31,30,31,30,31]

#define a procedure, how_many_days,
#that takes as input a number
#representing a month, and outputs
#the number of days in that month.

#how_many_days(1) => 31
#how_many_days(9) => 30

def how_many_days(number):

    days_in_month = [31,28,31,30,31,30,31,31,30,31,30,31]

    #print days_in_month[1]


    return days_in_month[number - 1]



