#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     18-03-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

#1 Gold Star

#The built-in <string>.split() procedure works
#okay, but fails to find all the words on a page
#because it only uses whitespace to split the
#string. To do better, we should also use punctuation
#marks to split the page into words.

#Define a procedure, split_string, that takes two
#inputs: the string to split and a string containing
#all of the characters considered separators. The
#procedure should output a list of strings that break
#the source string up by the characters in the
#splitlist.

#out = split_string("This is a test-of the,string separation-code!", " ,!-")
#print out => ['This', 'is', 'a', 'test', 'of', 'the', 'string', 'separation', 'code']

#out = split_string("After  the flood   ...  all the colors came out."," .")
#print out => ['After', 'the', 'flood', 'all', 'the', 'colors', 'came', 'out']

def split_string(source,splitlist):
    skip = []
    for entry in splitlist:
        skip.append(entry)

    temp_str = ""
    i = 0

    big_list = []
    while(i < len(source)):
       if(source[i] not in skip):
            temp_str += source[i]

       else:
            if( len(temp_str) != 0 ):
                big_list.append(temp_str) # append the previous string to list
                temp_str = ""  # make the temporary string empty
       i = i + 1   #increment i before reiterating


    return big_list


out = split_string("This is a test-of the,string separation-code!", " ,!-")
print out

out = split_string("After  the flood   ...  all the colors came out."," .")
print out

