#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     23-03-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python


import urllib
def get_page(url):
    try:
        return urllib.urlopen(url).read()
    except:
        return ""



def test_hash_function(function, keys, size):
    results = [0] * size
    keys_used = []
    for w in keys:
        if w not in keys_used:
            hv = function(w, size)
            result[hv] += 1
            keys_used.append(w)
    return results

def hash_string(keyword,buckets):
    sum = 0
    for c in keyword:
        sum += ord(c)

# better version
        # sum = (sum + (ord(c)) % buckets

    return sum % buckets

words = get_page("http://www.google.com")
counts = test_hash_function(hash_string, words, 12)
print (words)
print (counts)

