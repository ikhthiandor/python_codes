#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     19-03-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

import time

def exec_time(code): # a string will be passed
# as argument and eval will execute that string as code


    start = time.clock()
    result = eval(code)
    run_time = time.clock() - start
    return result, run_time

def fibo_d(n):
    current = 0
    after = 1

    for unused in range(0,n):
        current, after = after, current + after

    return current

#print exec_time('5 + 14')
#print exec_time('fibo_d(30)')