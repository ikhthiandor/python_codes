#-------------------------------------------------------------------------------
# Name:        module2
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     24-03-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

def simp_make_hashtable(nbuckets):
    return [["hello"]] * nbuckets

table = simp_make_hashtable(4)
print (table)

table.append(["world"])
table[2].append('monkey')
table[-2].append("gorky")

print (table)
print (table[0])
print (table[1])
print (table[2])
print (table[3])


