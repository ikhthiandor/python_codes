#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     14-03-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

m = 1
k = 45
if(k == 2):
    print "Nothing"

if (m ==2):
    print m
else:
    print "m is not 2"
