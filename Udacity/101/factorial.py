#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     01-03-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

#Define a procedure, factorial, that
#takes one number as its input
#and returns the factorial of
#that number.


def factorial(n):
    product = 1
    while(n>=1):
        product = product * n
        n = n-1

    return product

print factorial(821)


