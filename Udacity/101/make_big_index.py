#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     20-03-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

import time

def exec_time(code): # a string will be passed
# as argument and eval will execute that string as code


    start = time.clock()
    result = eval(code)
    run_time = time.clock() - start
    return result, run_time

def make_string(p):
    s = ""
    for e in p:
        s = s + e
    return s


def add_to_index(index,keyword,url):
    for entry in index:
        if entry[0] == keyword:
            entry[1].append(url)
            return
    index.append([keyword,[url]])

def lookup(index,keyword):

    for entry in index:
        if(entry[0] == keyword):
            return entry[1]

    return []

def make_big_index(size):
    index = []

    letters = ['a', 'a', 'a', 'a' , 'a', 'a', 'a', 'a' ]
    while len(index) < size:
        word = make_string(letters)
        add_to_index(index, word, "fake")
        for i in range( len(letters) - 1, 0, -1):
            if letters[i] < 'z':
                letters[i] = chr(ord(letters[i]) + 1)
                break
            else:
                letters[i] = 'a'
    return index

index10000 = make_big_index(10000000)
print exec_time(' lookup(index10000000, "aaaaaaaa") ')[1]
#print exec_time(' make_big_index(100000) ')[1]