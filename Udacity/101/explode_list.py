#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     12-04-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

#List Explosion


#Define a procedure, explode_list, that takes as inputs a list and a number, n.
#It should return a list which contains each of the elements of the input list,
#in the original order, but repeated n times.

def explode_list(p,n):
    output_ls = []

    for entry in p:
        for i in range(0,n):
            output_ls.append(entry)
    return output_ls





#For example,

print explode_list([1, 2, 3], 2)
#>>> [1, 1, 2, 2, 3, 3]

print explode_list([1, 0, 1], 0)
#>>> []

print explode_list(["super"], 5)
#>>> ["super", "super", "super", "super", "super"]