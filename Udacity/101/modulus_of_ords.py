#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     23-03-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

# keyword is a string
# buckets in integer
def mod_of_ords(keyword, buckets):
    for c in keyword:
        print( ord(c) % buckets )


mod_of_ords("adjective", 12)

