#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     01-04-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

dict = {"fruit": ["banana", "mango"],
        "flowers": ["rose", "tulip", "rhododendron"]
        }

for key in dict:
    for entry in dict[key]:
        print entry