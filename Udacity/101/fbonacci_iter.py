#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     28-03-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

def fibonacci(n):

    var1 = 0
    var2 = 1
    if n == 0 or n == 1:
        return n
    else:
        i = 2
        sum = 0
        while(i <= n):
            sum = var1 + var2
            var1 = var2
            var2 = sum
            i = i + 1
        return sum

for unused in range(0,3):
    print fibonacci(unused),