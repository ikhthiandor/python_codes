#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     28-03-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

def is_palindrome(s):
    if (s == "" or len(s) == 1):
        return True

    else:
        i = 0
        j = len(s) - 1
        if (s[i] == s[j]):
            return is_palindrome(s[i+1:j])
        else:
            return False




print is_palindrome('')
#>>> True
print is_palindrome('abab')
#>>> False
print is_palindrome('abba')
#>>> True
#print is_palindrome('a')
print is_palindrome('level')