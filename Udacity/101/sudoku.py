#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     14-03-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

#THREE GOLD STARS

#Sudoku [http://en.wikipedia.org/wiki/Sudoku]
#is a logic puzzle where a game
#is defined by a partially filled
#9 x 9 square of digits where each square
#contains one of the digits 1,2,3,4,5,6,7,8,9.
#For this question we will generalize
#and simplify the game.


#Define a procedure, check_sudoku,
#that takes as input a square list
#of lists representing an n x n
#sudoku puzzle solution and returns
#True if the input is a valid
#sudoku square and returns False
#otherwise.

#A valid sudoku square satisfies these
#two properties:

#   1. Each column of the square contains
#       each of the numbers from 1 to n exactly once.

#   2. Each row of the square contains each
#       of the numbers from 1 to n exactly once.

##correct = [[1,2,3],
##           [2,3,1],
##           [3,1,2]]
##
##correct2 = [[3,4,1,2,5], [1,3,4,5,2], [4,5,2,3,1], [2,1,5,4,3], [5,2,3,1,4]]
##
##correct3 = [[3,4,1,2,5], \
##                  [1,3,4,5,2], \
##                  [4,5,2,3,1], \
##                  [2,1,5,4,3], \
##                  [5,2,3,1,4]]
##
##correct4 = [1]
##
##correct5 = [[1,5,7,3,6,2,9,4,8], \
##                  [6,4,3,8,7,9,5,1,2], \
##                  [8,2,9,4,1,5,7,6,3], \
##                  [3,1,5,2,9,7,4,8,6], \
##                  [9,7,4,1,8,6,2,3,5], \
##                  [2,6,8,5,4,3,1,9,7], \
##                  [4,3,1,7,2,8,6,5,9], \
##                  [5,9,2,6,3,1,8,7,4], \
##                  [7,8,6,9,5,4,3,2,1]]
##
##
##incorrect = [[1,2,3,4],
##             [2,3,1,3],
##             [3,1,2,3],
##             [4,4,4,4]]
##
##
##
##incorrect2 = [[1,2,3,4], [4,3,2,1], [3,1,4,2], [2,4,3,1]]
##
##incorrect3 = [[1,2,3], [2,3,1], [3,1,5]]
##
##incorrect4 = [[1,2], \
##                  [2,1], \
##                  [1,2], \
##                  [2,1]]
##
##
##incorrect5 = [[1,2,3], \
##                  [2,4,1], \
##                  [3,1,2]]
##
##incorrect6 = [[1,2,3,4], \
##                  [2,3,1,3], \
##                  [3,1,2,3], \
##                  [4,4,4,4]]
##
##incorrect7 = [[1,2,3,4], \
##                  [4,3,2,1], \
##                  [3,1,4,2], \
##                  [2,4,3,1]]
##
##
##incorrect8 = [[1,2,3], \
##                  [2,3,1], \
##                  [3,1,5]]
##
##
##incorrect9 = [[1,5,7,3,6,2,9,4,8], \
##                  [6,4,3,8,7,9,5,1,2], \
##                  [8,2,9,4,1,5,7,6,3], \
##                  [3,1,5,2,9,7,4,8,6], \
##                  [9,7,4,1,9,6,2,3,5], \
##                  [2,6,8,5,4,3,1,9,7], \
##                  [4,3,1,7,2,8,6,5,9], \
##                  [5,9,2,6,3,1,8,7,4], \
##                  [7,8,6,9,5,4,3,2,1]]
##
##
##incorrect10 = [[2,2,2], \
##                  [2,2,2], \
##                  [2,2,2]]
##
##
##incorrect11 = [[2,3,4], \
##                  [3,4,2], \
##                  [4,2,3]]


##def print_columns(ls):
##    i = 0
##    j = 0
##    while (j < len(ls)):
##        while(i < len(ls)):
##            print correct[i][j]
##            i = i + 1
##        j = j + 1
##        i = 0

##def print_rows(ls):
##    for e in ls:
##        i = 0
##        while (i < len(ls)):
##            print e[i]
##            i = i + 1


def check_rows(ls):
    n = len(ls)
    for e in ls:
        flag = 0
        i = 0

        while (i < n and flag < n):
            j = 0
            while(j < n):
                if(j == flag):
                    j = j + 1
                    continue

                if(e[i] > n):
                    return False

                if(e[i] == e[j]):
                    return False
                else:
                    j = j + 1

            i = i + 1
            flag = flag + 1


    return True


def check_cols(ls):
    n = len(ls)
    rows = 0
    cols = 0
    flag = 0
    k = 0

    while (cols < n):
        while(rows < n and flag < n):

            while(k < n):
                if(flag == k):
                    k = k + 1
                    continue
                if(ls[rows][cols] > n):
                    return False

                if(ls[rows][cols] == ls[k][cols]):
                    return False
                else:
                    k = k + 1
            rows = rows + 1
            k = 0
            flag = flag + 1

        rows = 0
        flag = 0
        cols = cols + 1


    return True

def check_sudoku(ls):

    if(len(ls) < 1):
        return False

    bool_rows = check_rows(ls)
# if check_rows return True check_cols
# else don't need to check further and return False

##    if(bool_rows == True):
##        bool_cols = check_cols(ls)
##
##        if(bool_cols == True):
##            return True
##    else:
##        return False

    bool_rows = check_rows(ls)
    bool_cols = check_cols(ls)
    if(bool_rows and bool_cols):
        return True
    else:
        return False


## print check_sudoku([])







