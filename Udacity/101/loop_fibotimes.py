#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     29-03-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python



import time

def exec_time(code): # a string will be passed
# as argument and eval will execute that string as code


    start = time.clock()
    result = eval(code)
    run_time = time.clock() - start
    return result, run_time

def fibonacci(n):

    var1 = 0
    var2 = 1
    if n == 0 or n == 1:
        return n
    else:
        i = 2
        sum = 0
        while(i <= n):
            sum = var1 + var2
            var1 = var2
            var2 = sum
            i = i + 1
        return sum

def fibo_rec(n):
    if n == 0 or n == 1:
        return n
    else:
        return fibo_rec(n-1) + fibo_rec(n-2)

def spin_loop(n):
    i = 0
    while i < n:
        i = i + 1

# time to spin loop fibonacci(n) times
print exec_time('spin_loop(fibo_rec(34))')