#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Majibur Rahman
#
# Created:     21-03-2012
# Copyright:   (c) Majibur Rahman 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

ls = []
i = 97
while (i < 123):
    ls.append( chr(i) )
    i = i + 1

print ls

for e in ls:
    print ord(e)  % 5000
